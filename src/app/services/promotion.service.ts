import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private restangular: Restangular) { }
  
  getPromotions(): Observable<Promotion[]> {

  return this.restangular.all('promotions').getList();

}

  getPromotion(id: number): Observable<Promotion> {
   
  return this.restangular.one('promotions',id).get();
}

  getFeaturedPromotion(): Observable<Promotion> {
  
   /* return this.restangular.all(PROMOTIONS.filter((promotion) => promotion.featured)[0]).getList({featured:true});*/
   
   return this.restangular.all('promotions').getList({featured :true})
  .pipe(map(promotions => promotions[0])); 
  
  }

  
 
}

