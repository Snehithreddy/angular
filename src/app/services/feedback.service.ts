import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular } from 'ngx-restangular';
import {Feedback } from '../shared/feedback';
import { catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class FeedbackService {

  constructor(private restangular:Restangular) { }


      submitFeedback(feedback: Feedback): Observable<Feedback> {

        return this.restangular.all('feedback').post(feedback)
          .pipe(map(feedback => feedback))
           .pipe(catchError(error => error));
        
        }
        
        


}
